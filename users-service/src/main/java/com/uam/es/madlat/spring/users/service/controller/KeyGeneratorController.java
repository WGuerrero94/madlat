package com.uam.es.madlat.spring.users.service.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("key")
public class KeyGeneratorController {

	@GetMapping
	public void generateKey() throws NoSuchAlgorithmException, IOException {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		kpg.initialize(2048);
		KeyPair kp = kpg.generateKeyPair();
		Key pub = kp.getPublic();
		Key pvt = kp.getPrivate();
		
		String outFile = "madlat";
		FileOutputStream out = new FileOutputStream(outFile + ".key");
		out.write(pvt.getEncoded());
		out.close();
		 
		out = new FileOutputStream(outFile + ".pub");
		out.write(pub.getEncoded());
		out.close();
	}
	
	
//	private void readPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
//		/* Read all bytes from the private key file */
//		Path path = Paths.get("madlat.key");
//		byte[] bytes = Files.readAllBytes(path);
//		 
//		/* Generate private key. */
//		PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(bytes);
//		KeyFactory kf = KeyFactory.getInstance("RSA");
//		PrivateKey pvt = kf.generatePrivate(ks);
//	}
	
	
}
