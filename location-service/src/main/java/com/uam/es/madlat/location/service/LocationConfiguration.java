package com.uam.es.madlat.location.service;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LocationConfiguration {
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
}
