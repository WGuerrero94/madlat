package com.uam.es.madlat.location.service.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uam.es.madlat.base.bean.POIBean;
import com.uam.es.madlat.base.entity.Category;
import com.uam.es.madlat.base.projection.POIBasicBean;
import com.uam.es.madlat.location.service.service.POIService;

@RestController
@RequestMapping("/poi")
public class POIController {

	@Autowired
	POIService poiService;

	@Cacheable(value = "poi-elements-by-category")
	@GetMapping("/category/{category}")
	public List<POIBean> getPOIByCategory(@PathVariable("category") Category category) {
		return poiService.getPOIByCategory(category);
	}

	@Cacheable(value = "poi-elements")
	@GetMapping(produces = "application/json")
	public List<POIBean> getPOIs() {
		return poiService.getAllPOIs();
	}

	@Cacheable(value = "poi-elements")
	@GetMapping("/{id}")
	public POIBean getPOIById(@PathVariable("id") Long id) {
		return poiService.getPOI(id);
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	public POIBean addNewPOI(@RequestBody POIBean poi) {
		return poiService.savePOI(poi);
	}

	@PutMapping(path = "/{poiId}", consumes = "application/json", produces = "application/json")
	public POIBean upadte(@PathVariable("poiId") Long poiId, @RequestBody POIBean poi) {
		return poiService.savePOI(poi);
	}

	@Cacheable(value = "poi-basic-locations")
	@GetMapping(path = "/locations", produces = "application/json")
	public List<POIBasicBean> getPOIsMapBasicInfo() {

		return poiService.getAllBasicPOIs();
	}

	@GetMapping(path = "/locations/{category}", produces = "application/json")
	public List<POIBasicBean> getPOIsMapBasicInfoByCategory(@PathVariable("category") Category category) {
		return poiService.getAllBasicsByCategory(category);
	}

	@GetMapping("/random")
	public POIBean getRandomPOI() {
		return poiService.getRandomPOI();
	}

	@GetMapping("/favs")
	public List<POIBean> getFavourites(List<Long> ids) {
		return poiService.getPOIsInList(ids);
	}

	@GetMapping
	public String job() throws IOException {
		byte[] data = null;
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		BufferedImage bigImage = ImageIO.read(bis);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bigImage, "JPG", baos);
		byte[] smallImage = baos.toByteArray();
		return "";
	}

}
