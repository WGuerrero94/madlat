package com.uam.es.madlat.location.service.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.uam.es.madlat.base.bean.POIBean;
import com.uam.es.madlat.base.entity.Category;
import com.uam.es.madlat.base.entity.POI;
import com.uam.es.madlat.base.projection.POIBasicBean;
import com.uam.es.madlat.base.repository.POIRepository;

@Service
public class POIService {

	@Autowired
	private POIRepository poiRepository;

	@Autowired
	private ModelMapper modelMapper;


	@Transactional
	public List<POIBean> getPOIByCategory(Category category) {
		List<POI> poiList = poiRepository.findByCategory(category);

		return poiList.stream().map(poi -> modelMapper.map(poi, POIBean.class)).collect(Collectors.toList());
	}

//	@CacheEvict(cacheNames= {"poi-elements", "poi-elements-by-category"})
	@Transactional
	public POIBean savePOI(POIBean poi) {
		POI entityPOI = modelMapper.map(poi, POI.class);
		return modelMapper.map(poiRepository.save(entityPOI), POIBean.class);
	}


	public List<POIBean> getAllPOIs() {
		List<POI> poiList = poiRepository.findAll();

		return poiList.stream().map(poi -> modelMapper.map(poi, POIBean.class)).collect(Collectors.toList());
	}

	public POIBean getPOI(Long id) {
		Optional<POI> poiOptional = poiRepository.findById(id);
		if(poiOptional.isPresent())
			return modelMapper.map(poiOptional.get(), POIBean.class);
		return null;
	}

	public List<POIBasicBean> getAllBasicsByCategory(Category category) {
		return this.poiRepository.findByCategoryAndLocationNotNull(category);
	}
	
	public List<POIBasicBean> getAllBasicPOIs() {
		return this.poiRepository.findByLocationNotNull();
	}

	public POIBean getRandomPOI() {
		long count = poiRepository.count();
		int idx = (int) (Math.random() * count);
		Page<POI> page = poiRepository.findAll(PageRequest.of(idx, 1));
		POIBean poi = null;
		
		if (page.hasContent()) {
			poi = modelMapper.map(page.getContent().get(0), POIBean.class);
		}
		
		return poi;
	}

	public List<POIBean> getPOIsInList(List<Long> ids) {
		
		List<POI> poiList = poiRepository.findAllById(ids);
		return poiList.stream().map(poi -> modelMapper.map(poi, POIBean.class)).collect(Collectors.toList());
	}

}
