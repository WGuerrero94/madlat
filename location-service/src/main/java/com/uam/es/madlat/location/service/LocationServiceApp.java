package com.uam.es.madlat.location.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableEurekaClient
@EnableCaching
@ComponentScan(basePackages = {"com.uam.es.madlat"})
@EntityScan( basePackages = {"com.uam.es.madlat"} )
@EnableJpaRepositories("com.uam.es.madlat")
public class LocationServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(LocationServiceApp.class, args);
    }
}