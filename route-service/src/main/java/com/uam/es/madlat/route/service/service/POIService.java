package com.uam.es.madlat.route.service.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.uam.es.madlat.base.entity.Category;
import com.uam.es.madlat.base.entity.POI;

@FeignClient("location-service")
public interface POIService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/poi")
    List<POI> getAllPOIs();

    @RequestMapping(method = RequestMethod.PUT, value = "/poi/{poiId}", consumes = "application/json")
    POI update(@PathVariable("poiId") Long storeId, POI poi);

    
	@RequestMapping(method = RequestMethod.GET, value = "/poi/category/{category}")
    List<POI> getAllPOIsByCategory(@PathVariable("category") Category category);
}
