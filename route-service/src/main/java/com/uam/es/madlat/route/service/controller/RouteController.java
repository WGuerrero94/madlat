package com.uam.es.madlat.route.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uam.es.madlat.base.entity.Category;
import com.uam.es.madlat.base.entity.Route;
import com.uam.es.madlat.route.service.service.RouteService;

@RestController
@RequestMapping("/route")
public class RouteController {

	@Autowired
	private RouteService routeService;
	
	
	@GetMapping
	public List<Route> getAllRoutes() {
		return routeService.getAllRoutes();
	}
	
	@GetMapping("/creation")
	public Route getRoute(List<Category> category, Double timeToSpend){
		
		return null;
	}
	@GetMapping("test")
	public String testAuth() {
		return "Correct";
	}

}
