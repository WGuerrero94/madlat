package com.uam.es.madlat.listeners;

import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.data.model.POIBasic;
import com.uam.es.madlat.ui.location.list.POIListFragment;
import com.uam.es.madlat.ui.map.MapFragment;
import com.uam.es.madlat.utility.ImageConverter;

import org.json.JSONArray;

import java.util.Arrays;

public class POIMapResponseListener implements JSONArrayRequestListener {

    private MapFragment mapFragment;
    private GoogleMap mMap;

    public POIMapResponseListener(MapFragment mapFragment, GoogleMap mMap) {
        this.mapFragment = mapFragment;
        this.mMap = mMap;
    }


    @Override
    public void onResponse(JSONArray response) {
        if(response.length() > 0){
            String jsonStr = response.toString();

            POIBasic[] pois = ImageConverter.customGson.fromJson(jsonStr, POI[].class);

            for(POIBasic poi : pois){
                LatLng location = new LatLng(poi.getLocation().getLatitude(), poi.getLocation().getLongitude());
                Marker marker = mMap.addMarker(new MarkerOptions().position(location).title(poi.getName()));
                marker.setTag(poi);
            }

        } else {
            Toast.makeText(mapFragment.getActivity(), mapFragment.getActivity().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(ANError anError) {

    }
}
