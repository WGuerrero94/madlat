package com.uam.es.madlat.ui.map;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.Location;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapShowOneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapShowOneFragment extends Fragment implements OnMapReadyCallback {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_LAT = "latitude_param";
    private static final String ARG_LNG = "longitude_param";
    private static final String ARG_LAT_LNG = "latlng_param";
    private static final String ARG_TITLE = "title_param";

    private Double latitude;
    private Double longitude;
    private Location position;
    private String markerName;


    private GoogleMap mMap;

    public MapShowOneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param latitude Parameter 1.
     * @param longitude Parameter 2.
     * @param latLng Parameter of the position
     * @return A new instance of fragment MapShowOneFragment.
     */
    public static MapShowOneFragment newInstance(Double latitude, Double longitude, Location latLng, String name) {
        MapShowOneFragment fragment = new MapShowOneFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_LAT, latitude);
        args.putDouble(ARG_LNG, longitude);
        args.putSerializable(ARG_LAT_LNG, latLng);
        args.putString(ARG_TITLE, name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            latitude = getArguments().getDouble(ARG_LAT);
            longitude = getArguments().getDouble(ARG_LNG);
            position = (Location) getArguments().getSerializable(ARG_LAT_LNG);
            markerName = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_map_show_one, container, false);

        //GoolgeMap configuration
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_show_one);
        mapFragment.getMapAsync(this);

        return root;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Madrid and move the camera 40.416898, -3.703496
        LatLng mMarker = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(mMarker).title(markerName));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mMarker, 11));
    }
}
