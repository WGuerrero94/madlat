package com.uam.es.madlat.data.repository;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.uam.es.madlat.data.model.Category;
import com.uam.es.madlat.utility.ServerConfig;

public class POIRepository {

    public void getPOIsByCategory(Category category,  JSONArrayRequestListener listener){
        AndroidNetworking.get(ServerConfig.getURLBase() +"/poi/category/"+category.name())
                .setTag("categoryRequest-" + category.name())
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(listener);
    }

    public void getAllPOIs(JSONArrayRequestListener listener){
        AndroidNetworking.get(ServerConfig.getURLBase() +"/poi/")
                .setTag("poisRequest")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(listener);
    }

    public void getAllPOIBasics(JSONArrayRequestListener listener){
        AndroidNetworking.get(ServerConfig.getURLBase() +"/poi/locations")
                .setTag("poibasicRequest")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(listener);
    }

    public void getPOIById(Long id, JSONObjectRequestListener listener){
        AndroidNetworking.get(ServerConfig.getURLBase() +"/poi/"+ id)
                .setTag("poiByIdRequest")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(listener);
    }

    public void getRandomPOI(JSONObjectRequestListener listener){
        AndroidNetworking.get(ServerConfig.getURLBase() +"/poi/random")
                .setTag("poisRequest")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(listener);
    }



}
