package com.uam.es.madlat.data.repository;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.uam.es.madlat.data.model.POI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FavouriteRepository {

    public static final String PREFS_NAME = "MADLAT_APP";
    public static final String FAVORITES = "POI_Favorite";

    public FavouriteRepository() {
    }

    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<POI> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public void addFavorite(Context context, POI product) {
        List<POI> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<POI>();
        favorites.add(product);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, POI product) {
        ArrayList<POI> favorites = getFavorites(context);
        if (favorites != null) {
            favorites.remove(product);
            saveFavorites(context, favorites);
        }
    }

    public ArrayList<POI> getFavorites(Context context) {
        SharedPreferences settings;
        List<POI> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            POI[] favoriteItems = gson.fromJson(jsonFavorites,
                    POI[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<POI>(favorites);
        } else {
            favorites = new ArrayList<POI>();
            saveFavorites(context, favorites);
        }

        return (ArrayList<POI>) favorites;
    }
}
