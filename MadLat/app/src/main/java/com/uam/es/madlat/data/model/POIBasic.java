package com.uam.es.madlat.data.model;

import java.util.Objects;

public class POIBasic {
    private Long poiId;
    private String name;
    private Location location;
    private Category category;

    public Long getPoiId() {
        return poiId;
    }

    public void setPoiId(Long poiId) {
        this.poiId = poiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        POIBasic poiBasic = (POIBasic) o;
        return poiId.equals(poiBasic.poiId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(poiId);
    }
}
