package com.uam.es.madlat.ui.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.uam.es.madlat.MainActivity;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.Category;
import com.uam.es.madlat.data.repository.POIRepository;
import com.uam.es.madlat.listeners.POIListItemsResponseListener;
import com.uam.es.madlat.ui.poiinsert.POIInsertActivity;

public class HomeFragment extends Fragment{

    private HomeViewModel homeViewModel;
    private HomeFragment homeFragment;
    private MainActivity parent;
    private FloatingActionButton fab;
    private final int INSERT_POI_REQUEST_CODE = 1001;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeFragment = this;
        parent = (MainActivity) this.getActivity();
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        fab = root.findViewById(R.id.home_fab);
        fab.setOnClickListener(this.fabButtonClickListener());

        //Nature
        root.findViewById(R.id.card_gastronomy_container).setOnClickListener(homeButtonsListener(Category.GASTRONOMY, this));

        //Nature
        root.findViewById(R.id.card_nature_container).setOnClickListener(homeButtonsListener(Category.NATURE, this));

        //Culture
        root.findViewById(R.id.card_culture_container).setOnClickListener(homeButtonsListener(Category.CULTURE, this));

        //Art
        root.findViewById(R.id.card_art_container).setOnClickListener(homeButtonsListener(Category.ART, this));

        //History
        root.findViewById(R.id.card_history_container).setOnClickListener(homeButtonsListener(Category.HISTORY, this));

        return root;
    }

    private View.OnClickListener homeButtonsListener(final Category category, final HomeFragment homeFragment){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Call database and get info and after change the fragment
                POIRepository repo = new POIRepository();
                repo.getPOIsByCategory(category, new POIListItemsResponseListener(homeFragment));
            }

        };
    }

    private View.OnClickListener fabButtonClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(homeFragment.getActivity(), POIInsertActivity.class);
                startActivityForResult(intent, INSERT_POI_REQUEST_CODE);
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == INSERT_POI_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            new AlertDialog.Builder(parent)
                    .setMessage(parent.getString(R.string.poi_created))
                    .setPositiveButton("OK", null).create()
                    .show();
        }
    }
}
