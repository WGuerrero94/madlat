package com.uam.es.madlat.ui.register;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.uam.es.madlat.data.LoginDataSource;
import com.uam.es.madlat.data.LoginRepository;
import com.uam.es.madlat.data.RegisterDataSource;
import com.uam.es.madlat.data.RegisterRepository;
import com.uam.es.madlat.ui.login.LoginViewModel;

public class RegisterViewModelFactory implements ViewModelProvider.Factory{
    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new RegisterViewModel(RegisterRepository.getInstance(new RegisterDataSource()));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
