package com.uam.es.madlat.listeners;

import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.ui.location.profile.POIScrollingProfileFragment;
import com.uam.es.madlat.utility.ImageConverter;

import org.json.JSONObject;

public class OnePOIRequestListener implements JSONObjectRequestListener {

    private Fragment fragment;

    public OnePOIRequestListener(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void onResponse(JSONObject response) {
        if(response != null){
            String jsonStr = response.toString();

            POI poi = ImageConverter.customGson.fromJson(jsonStr, POI.class);

            POIScrollingProfileFragment poiScrollingProfileFragment = POIScrollingProfileFragment.newInstance(poi);
            FragmentTransaction ft = fragment.getParentFragmentManager().beginTransaction();
            ft.hide(fragment);
            ft.replace(R.id.nav_host_fragment, poiScrollingProfileFragment);
            ft.addToBackStack(null);
            ft.commit();

        }
    }

    @Override
    public void onError(ANError anError) {
        Toast.makeText(fragment.getActivity(), fragment.getActivity().getString(R.string.error_random_object), Toast.LENGTH_SHORT).show();
    }
}
