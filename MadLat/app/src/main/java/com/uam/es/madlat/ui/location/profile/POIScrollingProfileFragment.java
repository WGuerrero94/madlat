package com.uam.es.madlat.ui.location.profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.data.repository.FavouriteRepository;
import com.uam.es.madlat.listeners.OnListFragmentInteractionListener;
import com.uam.es.madlat.listeners.POIListItemListener;
import com.uam.es.madlat.ui.map.MapShowOneFragment;
import com.uam.es.madlat.utility.ImageConverter;

import java.util.List;


public class POIScrollingProfileFragment extends Fragment {

    private static final String ARG_ITEM = "ITEM";

    private POI poi;
    private OnListFragmentInteractionListener mListener;
    private FavouriteRepository favouriteRepository = new FavouriteRepository();

    private Fragment poiProfileFragment;

    public static POIScrollingProfileFragment newInstance(POI item){
        POIScrollingProfileFragment fragment = new POIScrollingProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        poiProfileFragment = this;
        if (getArguments() != null) {
            poi = (POI) getArguments().getSerializable(ARG_ITEM);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_location_profile, container, false);
        Toolbar toolbar = root.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        setUpFavButton(root);

        MaterialButton mapButton = root.findViewById(R.id.button_profile_map);
        mapButton.setOnClickListener(this.mapOnClickListener());

//        setUpRecycler(root);

        toolbar.setTitle(poi.getName());

        TextView region = root.findViewById(R.id.profile_region);
        region.setText(poi.getRegion());

        TextView description = root.findViewById(R.id.profile_description);
        description.setText(poi.getDescription());

        TextView url = root.findViewById(R.id.web_profile_data);
        url.setText(poi.getUrl());

        ImageView image = root.findViewById(R.id.header_profile_element);
        Bitmap bitmap = ImageConverter.byteArrayToBitmap(poi.getImage().getImage());
        image.setImageBitmap(bitmap);

        return root;
    }

    private void setUpFavButton(View root){
        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(this.favouriteButton(fab));
        List<POI> pois = favouriteRepository.getFavorites(poiProfileFragment.getContext());
        if(pois.contains(poi)){
            fab.setImageDrawable(getResources()
                    .getDrawable(R.drawable.ic_star_black_24dp, poiProfileFragment.getActivity().getTheme()));
        } else {
            fab.setImageDrawable(getResources()
                    .getDrawable(R.drawable.ic_star_border_black_24dp, poiProfileFragment.getActivity().getTheme()));
        }
    }

    private View.OnClickListener favouriteButton(final FloatingActionButton fab) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<POI> pois = favouriteRepository.getFavorites(poiProfileFragment.getContext());
                if(pois.contains(poi)){
                    fab.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_star_border_black_24dp, poiProfileFragment.getActivity().getTheme()));
                    favouriteRepository.removeFavorite(poiProfileFragment.getContext(), poi);
                } else {
                    fab.setImageDrawable(getResources()
                            .getDrawable(R.drawable.ic_star_black_24dp, poiProfileFragment.getActivity().getTheme()));
                    favouriteRepository.addFavorite(poiProfileFragment.getContext(), poi);
                }


            }
        };
    }

    /*private void setUpRecycler(View root) {
        View view = root.findViewById(R.id.list_horizontal_in_location_profile);
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(
                    new POIRecyclerViewAdapter(
                            R.layout.fragment_location_horizontal,
                            Collections.<POI>emptyList(),
                            mListener)
            );
        }
    }*/



    private View.OnClickListener mapOnClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MapShowOneFragment mapShowOneFragment = MapShowOneFragment.newInstance(poi.getLocation().getLatitude(),
                        poi.getLocation().getLongitude(), poi.getLocation(), poi.getName());
                FragmentTransaction ft = poiProfileFragment.getParentFragmentManager().beginTransaction();
                ft.hide(poiProfileFragment);
                ft.replace(R.id.nav_host_fragment, mapShowOneFragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = new POIListItemListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
