package com.uam.es.madlat.data.model;

import java.util.List;

public class Route {

    private String name;
    private List<POI> poiList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<POI> getPoiList() {
        return poiList;
    }

    public void setPoiList(List<POI> poiList) {
        this.poiList = poiList;
    }
    public void addPOI(POI poi) {
        this.getPoiList().add(poi);
    }

}
