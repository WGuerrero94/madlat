package com.uam.es.madlat.listeners;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.ui.location.profile.POIScrollingProfileFragment;

public class POIListItemListener implements OnListFragmentInteractionListener<POI> {
    private final Fragment fragment;

    public POIListItemListener(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void onListFragmentInteraction(POI item) {
        POIScrollingProfileFragment poiScrollingProfileFragment = POIScrollingProfileFragment.newInstance(item);
        FragmentTransaction ft = fragment.getParentFragmentManager().beginTransaction();
        ft.hide(fragment);
        ft.replace(R.id.nav_host_fragment, poiScrollingProfileFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}
