package com.uam.es.madlat.data.model;

public enum Category {
    GASTRONOMY, NATURE, ART, CULTURE, HISTORY
}
