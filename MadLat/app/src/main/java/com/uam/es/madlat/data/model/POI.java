package com.uam.es.madlat.data.model;

import java.io.Serializable;

public class POI extends POIBasic implements Serializable {
    private String region;
    private ImageByte image;
    private String description;
    private String url;

    public POI() {
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public ImageByte getImage() {
        return image;
    }

    public void setImage(ImageByte image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
