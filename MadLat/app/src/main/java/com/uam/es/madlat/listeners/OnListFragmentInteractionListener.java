package com.uam.es.madlat.listeners;

public interface OnListFragmentInteractionListener<T> {
    void onListFragmentInteraction(T item);
}
