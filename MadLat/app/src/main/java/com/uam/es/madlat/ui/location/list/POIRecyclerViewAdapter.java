package com.uam.es.madlat.ui.location.list;

import android.graphics.Bitmap;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;

import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.listeners.OnListFragmentInteractionListener;
import com.uam.es.madlat.utility.ImageConverter;

import java.util.List;

public class POIRecyclerViewAdapter extends RecyclerView.Adapter<POIRecyclerViewAdapter.ViewHolder> {

    private final List<POI> mValues;
    private final OnListFragmentInteractionListener mListener;

    @LayoutRes
    private final int mLayoutResource;

    public POIRecyclerViewAdapter(@LayoutRes int layoutResource, List<POI> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mLayoutResource = layoutResource;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mLayoutResource, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getName());
        holder.mNameView.setText(mValues.get(position).getDescription());
        Bitmap bitmap = ImageConverter.byteArrayToBitmap(mValues.get(position).getImage().getImage());
        holder.mImageView.setImageBitmap(bitmap);
        holder.mItem = mValues.get(position);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mNameView;
        public final ImageView mImageView;
        public POI mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mNameView = (TextView) view.findViewById(R.id.content);
            this.mImageView = (ImageView) view.findViewById(R.id.card_list_image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
