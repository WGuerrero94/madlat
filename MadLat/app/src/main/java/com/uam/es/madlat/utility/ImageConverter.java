package com.uam.es.madlat.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.uam.es.madlat.R;

import java.io.ByteArrayOutputStream;

public class ImageConverter {
    public static Gson customGson = new GsonBuilder().registerTypeHierarchyAdapter(byte[].class,
            new ByteArrayToBase64TypeAdapter()).create();
    private static final int defaultImage = R.drawable.ic_add_image_black;
    private final Drawable defaultDrawable;

    public ImageConverter(Context context) {
        defaultDrawable = context.getResources().getDrawable(defaultImage, context.getTheme());
    }

    public byte[] imageViewToByteArray(ImageView imageView){

        if(imageView.getDrawable() != defaultDrawable){
            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            return baos.toByteArray();
        }
        return null;
    }

    public static Bitmap byteArrayToBitmap(byte[] imageByte){

        if(imageByte.length > 0){
            return BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
        }

        return null;
    }




}
