package com.uam.es.madlat.ui.map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.data.model.POIBasic;
import com.uam.es.madlat.data.repository.POIRepository;
import com.uam.es.madlat.listeners.OnePOIRequestListener;
import com.uam.es.madlat.listeners.POIMapResponseListener;
import com.uam.es.madlat.ui.location.profile.POIScrollingProfileFragment;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
    private GoogleMap mMap;

    private volatile List<POI> poiList = new ArrayList<>();
    private POIRepository poiRepository = new POIRepository();

    private MapFragment mapFragment;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_map, container, false);
        mapFragment = this;
        //GoolgeMap configuration
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return root;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Madrid and move the camera 40.416898, -3.703496
        LatLng madrid = new LatLng(40.416898, -3.703496);
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(madrid));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(madrid, 11));
        poiRepository.getAllPOIBasics(new POIMapResponseListener(this, mMap));
        mMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        POIBasic poi = (POIBasic) marker.getTag();
        if(poi != null && poi.getPoiId() != null){
            poiRepository.getPOIById(poi.getPoiId(), new OnePOIRequestListener(mapFragment));
        }
    }
}
