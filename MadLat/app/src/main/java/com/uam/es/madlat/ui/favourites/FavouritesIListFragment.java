package com.uam.es.madlat.ui.favourites;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.data.repository.FavouriteRepository;
import com.uam.es.madlat.listeners.OnListFragmentInteractionListener;
import com.uam.es.madlat.listeners.POIListItemListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FavouritesIListFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;

    private List<POI> pois = new ArrayList<>();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FavouritesIListFragment() {
    }

    public static FavouritesIListFragment newInstance() {
        FavouritesIListFragment fragment = new FavouritesIListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FavouriteRepository favouriteRepository = new FavouriteRepository();
        this.pois = favouriteRepository.getFavorites(this.requireContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(
                    new FavouritesRecyclerViewAdapter(
                            R.layout.fragment_location_vertical,
                            pois,
                            mListener));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = new POIListItemListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
