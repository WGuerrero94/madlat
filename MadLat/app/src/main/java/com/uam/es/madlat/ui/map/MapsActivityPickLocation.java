package com.uam.es.madlat.ui.map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uam.es.madlat.R;

public class MapsActivityPickLocation extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    public static final String LATITUDE_PARAM = "latitude";
    public static final String LONGITUDE_PARAM = "longitude";

    private GoogleMap mMap;
    private Button buttonSend;
    private LatLng picked;
    private Marker myMarker = null;

    private AppCompatActivity me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_pick_location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_picker);
        mapFragment.getMapAsync(this);
        buttonSend = findViewById(R.id.button_send_location);
        buttonSend.setOnClickListener(this.onClickListener());

        me = this;
    }

    private View.OnClickListener onClickListener(){
        return new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();

                resultIntent.putExtra(LATITUDE_PARAM, myMarker.getPosition().latitude);
                resultIntent.putExtra(LONGITUDE_PARAM, myMarker.getPosition().longitude);

                setResult(Activity.RESULT_OK, resultIntent);
                me.finish();
            }
        };
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng madrid = new LatLng(40.416898, -3.703496);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(madrid, 11));

        mMap.setOnMapClickListener(this);
    }


    @Override
    public void onMapClick(LatLng latLng) {

        if(myMarker != null){
            myMarker.remove();
        }

        myMarker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));

        buttonSend.setVisibility(View.VISIBLE);
    }


}
