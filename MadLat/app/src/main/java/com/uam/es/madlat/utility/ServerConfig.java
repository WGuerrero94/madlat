package com.uam.es.madlat.utility;

public class ServerConfig {

    private static final boolean INTERNAL_NETWORK = true;

    public static String getURLBase(){
        return INTERNAL_NETWORK ? "http://192.168.0.19:9080":"http://46.27.70.13";
    }

}