package com.uam.es.madlat.data.model;

import android.media.Image;

import java.util.List;

public class DetailedPOI extends POI{

    private List<ImageByte> images;

    public List<ImageByte> getImages() {
        return images;
    }

    public void setImages(List<ImageByte> images) {
        this.images = images;
    }

    public void addImage(ImageByte image){
        this.images.add(image);
    }
}
