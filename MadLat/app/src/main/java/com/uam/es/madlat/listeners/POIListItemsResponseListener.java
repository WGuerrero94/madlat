package com.uam.es.madlat.listeners;

import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.ui.location.list.POIListFragment;
import com.uam.es.madlat.utility.ImageConverter;

import org.json.JSONArray;

import java.util.Arrays;

public class POIListItemsResponseListener implements JSONArrayRequestListener {

    private Fragment homeFragment;

    public POIListItemsResponseListener(Fragment homeFragment) {
        this.homeFragment = homeFragment;
    }

    @Override
    public void onResponse(JSONArray response) {
        if(response.length() > 0){
            String jsonStr = response.toString();

            POI[] pois = ImageConverter.customGson.fromJson(jsonStr, POI[].class);

            POIListFragment poiListFragment = POIListFragment.newInstance(Arrays.asList(pois));
            FragmentTransaction ft = homeFragment.getParentFragmentManager().beginTransaction();
            ft.hide(homeFragment);
            ft.replace(R.id.nav_host_fragment, poiListFragment);
            ft.addToBackStack(null);
            ft.commit();
        } else {
            Toast.makeText(homeFragment.getActivity(), homeFragment.getActivity().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onError(ANError anError) {
        Toast.makeText(homeFragment.getContext(), "Error obtaining the data", Toast.LENGTH_SHORT).show();
    }
}
