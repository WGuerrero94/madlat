package com.uam.es.madlat.ui.poiinsert;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.maps.model.LatLng;
import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.Category;
import com.uam.es.madlat.data.model.ImageByte;
import com.uam.es.madlat.data.model.Location;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.ui.map.MapsActivityPickLocation;
import com.uam.es.madlat.utility.ImageConverter;
import com.uam.es.madlat.utility.ServerConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class POIInsertActivity extends AppCompatActivity {

    private Spinner mSpinner;
    private ImageView mainImage;

    private EditText locationText;

    private LatLng poiLocation;

    private final int IMAGE_PICK_CODE = 1000;
    private final int PERMISSION_CODE = 1001;

    private final int LOCATION_REQUEST_CODE = 1008;

    private Activity myActivity;

    private EditText description;
    private EditText name;
    private EditText url;
    private EditText region;

    private ImageConverter converter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_insert);
        myActivity = this;
        converter = new ImageConverter(this);

        setUpSpinner();
        setUpMainImage();
        setUpLocationButton();
        setUpSendInfo();
    }

    private void setUpSendInfo(){
        description = findViewById(R.id.poi_edit_description);
        name = findViewById(R.id.poi_edit_name);
        url = findViewById(R.id.poi_edit_url);
        region = findViewById(R.id.poi_edit_region);
        locationText = findViewById(R.id.poi_location_text_picked);


        Button sendInfo = findViewById(R.id.button_create_poi);
        sendInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                POI poi  = new POI();
                poi.setName(name.getText().toString());
                poi.setDescription(description.getText().toString());

                String[] latLong =  locationText.getText().toString().split(",");
                poi.setLocation(new Location(Double.parseDouble(latLong[0]), Double.parseDouble(latLong[1])));

                poi.setRegion(region.getText().toString());

                ImageByte imageByte = new ImageByte();
                imageByte.setImage(converter.imageViewToByteArray(mainImage));

                poi.setImage(imageByte);
                poi.setUrl(url.getText().toString());

                TextView option = (TextView) mSpinner.getSelectedView();
                poi.setCategory(Category.valueOf(option.getText().toString()));

                JSONObject jsonObject = null;
                try {
                    String json = ImageConverter.customGson.toJson(poi); //convert
                    jsonObject = new JSONObject(json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AndroidNetworking.post(ServerConfig.getURLBase() +"/poi")
                        .addJSONObjectBody(jsonObject)
                        .setTag("create-poi")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                //Toast.makeText(myActivity, "The element was created", Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                myActivity.finish();
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                                Toast.makeText(myActivity, "Error creating the element", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }

    private void setUpLocationButton() {
        Button selectLocation = findViewById(R.id.button_select_location);
        locationText = findViewById(R.id.poi_location_text_picked);
        selectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myActivity, MapsActivityPickLocation.class);
                startActivityForResult(intent, LOCATION_REQUEST_CODE);
            }
        });
    }

    private void setUpSpinner(){
        List<Category> elements = Arrays.asList(Category.values());
        mSpinner = findViewById(R.id.category_spinner);
        ArrayAdapter<Category> adp = new ArrayAdapter<>(myActivity, android.R.layout.simple_spinner_dropdown_item, elements);
        mSpinner.setAdapter(adp);
    }

    private void setUpMainImage(){
        mainImage = findViewById(R.id.description_image);
        mainImage.setOnClickListener(imageOnClickListener(IMAGE_PICK_CODE));
    }

    private View.OnClickListener imageOnClickListener(final int requestCode){
        return new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Check runtime permission
                if(myActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED){
                    //Ask for permission
                    String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                    myActivity.requestPermissions(permissions, PERMISSION_CODE);
                } else {
                    //system os is less than Marshmallow or permission already granted
                    pickImageFromGallery(requestCode);
                }
            }
        };
    }
    private void pickImageFromGallery(int requestCode){
        //Intent to pick image
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, requestCode);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case IMAGE_PICK_CODE:
                    mainImage.setImageURI(data.getData());
                    break;

                case LOCATION_REQUEST_CODE:
                    double latitude = data.getDoubleExtra(MapsActivityPickLocation.LATITUDE_PARAM, 200.0D);
                    double longitude = data.getDoubleExtra(MapsActivityPickLocation.LONGITUDE_PARAM, 200.0D);

                    if (latitude < 180.0D && longitude < 180.0D) {

                        locationText.setText(latitude + ", " + longitude);
                        this.poiLocation = new LatLng(latitude, longitude);
                    }

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
                Toast.makeText(myActivity, "The permission is mandatory", Toast.LENGTH_SHORT).show();
            } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                pickImageFromGallery(IMAGE_PICK_CODE);
            }
        }
    }
}
