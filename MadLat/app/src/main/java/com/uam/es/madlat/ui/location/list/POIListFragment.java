package com.uam.es.madlat.ui.location.list;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uam.es.madlat.R;
import com.uam.es.madlat.data.model.POI;
import com.uam.es.madlat.listeners.OnListFragmentInteractionListener;
import com.uam.es.madlat.listeners.POIListItemListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class POIListFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;

    private List<POI> pois = new ArrayList<>();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    private POIListFragment() {
    }

    public static POIListFragment newInstance(List<POI> pois) {
        POIListFragment fragment = new POIListFragment();
        fragment.pois = pois;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(
                    new POIRecyclerViewAdapter(
                            R.layout.fragment_location_vertical,
                            pois,
                            mListener));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = new POIListItemListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
