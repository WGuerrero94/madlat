package com.uam.es.madlat.base.bean;

import com.uam.es.madlat.base.entity.Category;

import lombok.Data;

@Data
public class POIBean {
	
	private Long poiId;
	
	private String name;
	
    private LocationBean location;
	
	private Category category;
	
	private String region;
	
    private String description;
    
	private ImageBean image;
	
	private String url;
}
