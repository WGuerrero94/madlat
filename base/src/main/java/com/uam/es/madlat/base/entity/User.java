package com.uam.es.madlat.base.entity;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import lombok.Data;

@Entity
@Table(name = "MADLAT_USER")
@Data
public class User {
	
	@Id
	private String username;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String password;
	
	@OneToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Image image;
	
	@CreatedDate
	@Column(nullable = false)
	private Date registrationDate;
	
}
