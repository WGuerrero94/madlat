package com.uam.es.madlat.base.bean;

import lombok.Data;

@Data
public class ImageBean {
	private Long imageId;
	
	private byte[] image;
}
