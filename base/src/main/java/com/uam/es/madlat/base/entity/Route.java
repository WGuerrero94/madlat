package com.uam.es.madlat.base.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import lombok.Data;

@Entity
@Table(name = "ROUTE")
@Data
public class Route {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@JoinTable(
	        name = "REL_ROUTES_POIS",
	        joinColumns = @JoinColumn(name = "FK_ROUTE", nullable = false),
	        inverseJoinColumns = @JoinColumn(name="FK_POI", nullable = false)
		)
	@ManyToMany(cascade = CascadeType.ALL, fetch= FetchType.EAGER)
	private List<POI> pois;
}
