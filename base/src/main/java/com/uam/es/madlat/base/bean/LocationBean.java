package com.uam.es.madlat.base.bean;

import lombok.Data;

@Data
public class LocationBean {
	
	private Double latitude;

	private Double longitude;
}
