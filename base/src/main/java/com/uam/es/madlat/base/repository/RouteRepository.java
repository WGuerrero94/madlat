package com.uam.es.madlat.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uam.es.madlat.base.entity.Route;


public interface RouteRepository extends JpaRepository<Route, Long> {
	
	
}
