package com.uam.es.madlat.base.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uam.es.madlat.base.entity.Category;
import com.uam.es.madlat.base.entity.POI;
import com.uam.es.madlat.base.projection.POIBasicBean;


@Repository
public interface POIRepository extends JpaRepository<POI, Long>{
	

	List<POI> findByCategory(Category category);
	
	List<POIBasicBean> findByLocationNotNull();
	
	List<POIBasicBean> findByCategoryAndLocationNotNull(Category category);

}
