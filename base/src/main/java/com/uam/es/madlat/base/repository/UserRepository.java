package com.uam.es.madlat.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uam.es.madlat.base.entity.User;


public interface UserRepository extends JpaRepository<User, String> {

	
}
