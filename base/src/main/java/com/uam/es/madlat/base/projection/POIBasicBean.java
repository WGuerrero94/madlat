package com.uam.es.madlat.base.projection;

import com.uam.es.madlat.base.entity.Category;
import com.uam.es.madlat.base.entity.Location;

public interface POIBasicBean {
	
	Long getPoiId();
	
	String getName();
	
	Location getLocation();
	
	Category getCategory();
	
}
