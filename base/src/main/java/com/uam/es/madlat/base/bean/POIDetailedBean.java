package com.uam.es.madlat.base.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class POIDetailedBean extends POIBean{
	
	@Getter
	@Setter
	private List<ImageBean> images;
}
