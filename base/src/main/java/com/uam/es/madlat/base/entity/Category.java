package com.uam.es.madlat.base.entity;

public enum Category {
	GASTRONOMY, NATURE, ART, CULTURE, HISTORY
}
