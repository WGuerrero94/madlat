package com.uam.es.madlat.base.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "POI")
@Data
public class POI {
	
	@Id
	@GeneratedValue
	private Long poiId;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String region;
	
	@Lob
	@Column(nullable = false)
    private String description;
    
	@OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Image image;
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Location location;
	
	@Enumerated(EnumType.ORDINAL)
	private Category category;
	
	@Column
	private String url;
	
	@OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Image miniature;
	
	@Column
	private Integer timeToSpend;
}
