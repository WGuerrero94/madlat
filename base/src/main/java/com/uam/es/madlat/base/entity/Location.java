package com.uam.es.madlat.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Entity
@Table(name = "location")
@Data
public class Location {
	
	@Id
	@GeneratedValue
	private Long locationId;
	
	@Column
	private Double latitude;

	@Column
	private Double longitude;
}
