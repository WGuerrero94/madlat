package com.uam.es.madlat.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;

@Entity
@Table(name = "image")
@Data
public class Image {
	
	@Id
	@GeneratedValue
	private Long imageId;
	
	@Column(nullable = false)
	@Lob
	@Type(type="org.hibernate.type.BinaryType")
	private byte[] image;
}
